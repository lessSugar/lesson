// miniprogram/pages/showNews/showNews.js
const db = wx.cloud.database({
  env: 'dev-9cjx9'
});
Page({

  /**
   * 页面的初始数据
   */
  data: {
    newsId: null,
    newsBody: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let newsId = options.newsId
    let _this = this;
    db.collection('news').where({
      _id: newsId
    })
    .get({
      success: function(res) {
        _this.setData({
          newsBody: res.data[0].body
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})