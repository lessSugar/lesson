// miniprogram/pages/editProjectInfo/editProjectInfo.js
import utils from '../../utils/util'
const db = wx.cloud.database({
  env: 'dev-9cjx9'
})
Page({
  textChange(e){
    this.data.textValue = e.detail.value;
    console.log(this.data.textValue)
  },
  backToSaveProjectInfoPage(){
    db.collection('temp_house_introduce').add({
      data: {tempContents:this.data.contents},
      success(res) {
        let pages = getCurrentPages();
        let prePage = pages[pages.length - 2];
        prePage.setData({
          projectIntroduce: res._id
        })
        console.log(prePage.data)
        wx.navigateBack({
          delta:1
        })
        _this.setData({
          contents: []
        })
      }
    })
  },
  /**
   * 页面的初始数据
   */
  data: {
    textValue:'',
    contents: []
  },
  addSection(){
    this.data.contents.push({
      type: 0,
      content: null
    })
    this.setData({
      contents: this.data.contents
    })
  },
  addImg(){
    this.data.contents.push({
      type: 1,
      content: null
    })
    
    this.setData({
      contents: this.data.contents
    })
  },
  sectionValueChange(e){
    let editIndex = e.currentTarget.dataset.index;
    this.setData({
      [`contents[${editIndex}].content`]: e.detail.value
    })
  },
  uploadImg(e){
    let _this = this
    let handleIndex = e.currentTarget.dataset.index
    wx.chooseImage({
      success: function(res) {
        let tempPath = res.tempFilePaths[0]
        let splitPaths = tempPath.split(".")
        let proImgCloudPath =   utils.getUUID() + "." + splitPaths[splitPaths.length - 1];
        wx.cloud.uploadFile({
          // 指定上传到的云路径
          cloudPath: proImgCloudPath,
          // 指定要上传的文件的小程序临时文件路径
          filePath: tempPath,
          // 成功回调
          success: res => {
           _this.setData({
             [`contents[${handleIndex}].content`]: res.fileID
           })
          }
        })
      },
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let _this = this
    if(!!options.msg){
      db.collection('temp_house_introduce').where({
        // gt 方法用于指定一个 "大于" 条件，此处 _.gt(30) 是一个 "大于 30" 的条件
            _id: options.msg
          })
          .get({
            success: function(res) {
              _this.setData({
                contents: res.data[0].tempContents
              })
              console.log(_this.data.contents)
            }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})