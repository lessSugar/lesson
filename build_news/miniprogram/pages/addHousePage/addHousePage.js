// miniprogram/pages/addHousePage/addHousePage.js
const db = wx.cloud.database({
  env: 'dev-9cjx9'
})
const util = require('../../utils/util.js')
const app = getApp()
Page({
  //添加一个项目图片录入路径输入框
  addProjectImgInput() {
    this.setData({
      projectImgSrcs: this.data.projectImgSrcs.concat('')
    })
  },
  deleteProjectImgItem(e) {
    let delIndex = e.currentTarget.dataset.index - 1;
    let newDatas = this.data.projectImgNums.splice(delIndex, 1);
    // this.setData({
    //   projectImgNums: newData
    // })
  },
  //保存房屋信息
  saveInfos() {
    let houseId = util.getUUID();
    if(!!app.globalData.userInfo){
      this.setData({
        userInfo: app.globalData.userInfo
      })
      let nickName = this.data.userInfo.nickName
      let data = {
        nickName: nickName,
      }
    }else {
      wx.navigateTo({
        url: '/pages/startPage/startPage?needBack=1',
      })
    }
    let saveData = {
      houseId: houseId,
      houseName: this.data.houseName,
      houseAddr: this.data.houseAddr,
      faceImgSrc: this.data.faceImgSrc,
      projectIntroduce: this.data.projectIntroduce,
      projectImgSrcs: this.data.projectImgSrcs,
      areaCode: this.data.areaCode,
      wxNumber: this.data.wxNumber,
      phoneNumber: this.data.phoneNumber,
      saveUser: this.data.userInfo.nickName
    }
    if (!saveData.houseName || !saveData.projectIntroduce) {
      this.setData({
        error: '请输入项目名称和项目简介信息'
      })
      return;
    }
    if (!saveData.projectIntroduce) {
      this.setData({
        error: '请填写项目简介信息'
      })
      return;
    }
    if (!this.data.faceImgSrc) {
      this.setData({
        error: '请选择封面图片'
      })
      return;
    }
    if(!this.data.phoneNumber && !this.data.wxNumber){
      this.setData({
        error: '请输入微信号码或电话号码!'
      })
      return;
    }
    let uploadImgsLength = this.data.projectImgSrcs.length;
    if(uploadImgsLength < 6){
      this.setData({
        error: '请至少上传6张项目详情图片!'
      })
      return;
    }
    let fileTypeArr = this.data.faceImgSrc.split(".");
    let fileType = fileTypeArr[fileTypeArr.length - 1];
    let faceImgCloudPath = this.data.houseName + '/' + 'pFace_' + houseId + "." + fileType;
    wx.cloud.uploadFile({
      // 指定上传到的云路径
      cloudPath: faceImgCloudPath,
      // 指定要上传的文件的小程序临时文件路径
      filePath: this.data.faceImgSrc,
      // 成功回调
      success: res => {
        console.log('上传成功', res)
        saveData.faceImgSrc = res.fileID
      }
    })
    let _this = this;
    let projectUploadImgUrls = [];
    for (let i = 0; i < uploadImgsLength; i++) {
      let fileTypeArr = this.data.projectImgSrcs[i].split(".");
      let fileType = fileTypeArr[fileTypeArr.length - 1];
      let proImgCloudPath = this.data.houseName + '/' + 'pPro_' + i + '_' + houseId + "." + fileType;
      wx.cloud.uploadFile({
        // 指定上传到的云路径
        cloudPath: proImgCloudPath,
        // 指定要上传的文件的小程序临时文件路径
        filePath: _this.data.projectImgSrcs[i],
        // 成功回调
        success: res => {
          console.log('上传成功', res)
          projectUploadImgUrls.push(res.fileID)
          if(projectUploadImgUrls.length == uploadImgsLength){
            saveData.projectImgSrcs = projectUploadImgUrls;
            db.collection('hot_sale_house_info').add({
              data: saveData,
              success(res) {
                _this.setData({
                  success: '恭喜您,保存成功!'
                })
                _this.setData({
                  houseName: '',
                  houseAddr: '',
                  projectIntroduce: '',
                  projectImgSrcs: [],
                  faceImgSrc: null,
                  areaCode: 0,
                  hasChooseUploadImgsNum:0,
                  wxNumber:'',
                  phoneNumber:''
                })
              }
            })
          }
        }
      })
    }
  },
  //上传项目的封面图片，暂存，等保存项目信息的时候保存至云存储
  uploadFaceImg() {
    let _this = this;
    wx.chooseImage({
      success: function(res) {
        let updatedImgSrc = res.tempFilePaths[0];
        _this.setData({
          faceImgSrc: updatedImgSrc
        })
      },
    })
  },
  uploadProjectImgs() {
    let _this = this;
    wx.chooseImage({
      success: function(res) {
        for (let i = 0; i < res.tempFilePaths.length; i++) {
          _this.setData({
            projectImgSrcs: _this.data.projectImgSrcs.concat(res.tempFilePaths[i]),
            hasChooseUploadImgsNum: _this.data.hasChooseUploadImgsNum + 1
          })
        }
        console.log(_this.data.projectImgSrcs)
      },
    })
  },
  changeHouseName(e) {
    this.setData({
      houseName: e.detail.value
    })
  },
  changeHouseAddr(e){
    this.setData({
      houseAddr: e.detail.value
    })
  },
  changeProjectIntroduce(e) {
    this.setData({
      projectIntroduce: e.detail.value
    })
  },
  changeProjectImgItemSrc(e) {
    let changeImgItemIndex = e.currentTarget.dataset.changeindex;
    let imgSrcs = this.data.projectImgSrcs;
    imgSrcs[changeImgItemIndex] = e.detail.value;
    this.setData({
      projectImgSrcs: imgSrcs
    })
    console.log(this.data.projectImgSrcs);
  },
  changeWxNumber(e){
    this.setData({
      wxNumber: e.detail.value
    })
  },
  changePhoneNumber(e){
    let val = e.detail.value.replace(/\D/g, '')
    this.setData({
      phoneNumber: val
    })
  },
  bindPickerChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      areaCode: e.detail.value
    })
  },
  //跳转到编辑项目简介页面
  editProjectInfo(){
    wx.navigateTo({
      url: '../editProjectInfo/editProjectInfo?msg='+this.data.projectIntroduce,
    })
  },
  /**
   * 页面的初始数据
   */
  data: {
    houseName: '',
    houseAddr: '',
    projectIntroduce: null,
    projectImgSrcs: [],
    faceImgSrc: null,
    areaCodes: ['北三县', '雄安新区', '环京楼盘','其他楼盘'],
    areaCode: '0',
    hasChooseUploadImgsNum:0,
    userInfo: {},
    nickName: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let _this = this;
    if(!!app.globalData.userInfo){
      this.setData({
        userInfo: app.globalData.userInfo
      })
      let nickName = this.data.userInfo.nickName
      let data = {
        nickName: nickName,
      }
    }else {
      wx.navigateTo({
        url: '/pages/startPage/startPage?needBack=1',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})