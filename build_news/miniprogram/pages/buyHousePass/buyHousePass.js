// miniprogram/pages/buyHousePass/buyHousePass.js
const db = wx.cloud.database({
  env: 'dev-9cjx9'
})
Page({

  /**
   * 页面的初始数据
   */
  data: {
    area: '',
    acre: '',
    roomType: '',
    price: null,
    isShow: true,
    selectPayIndex: 3,
    selectPayName: '请选择付款方式',
    phone: '',
    wxNumber: '',
    payTypes: [ '贷款','全款','全款分期','请选择付款方式'],
    key: {
      type: String,
      value: 'id'
    },
    rules: [{
      name: 'area',
      rules: {required: true, message: '单选列表是必选项'},
    }, {
        name: 'acre',
        rules: {required: true, message: '多选列表是必选项'},
    }, {
        name: 'price',
        rules: [{required: true, message: 'mobile必填'}, {mobile: true, message: 'mobile格式不对'}],
    }, {
        name: 'phone',
        rules: {required: true, message: '验证码必填'},
    }, {
        name: 'wxNumber',
        rules: {required: true, message: 'idcard必填'},
    }],
    countryCodes: ["+86", "+80", "+84", "+87"],
    countryCodeIndex: 0,
    text: {
      type: String,
      value: 'name'
    },
    error: null
  },
  bindPickerChange(changeIndex){
      let index = parseInt(changeIndex.detail.value)
      this.setData({
        selectPayIndex: index,
        selectPayName: this.data.payTypes[index]
      })
  },
  areaChange(e){
      this.setData({
        area: e.detail.value
      })
  },
  acreChange(e){
    let val = e.detail.value.replace(/\D/g, '')
    this.setData({
      acre: val
    })
  },
  roomTypeChange(e){
    this.setData({
      roomType: e.detail.value
    })
  },
  priceChange(e){
    let money = null;
    if (/^(\d?)+(\.\d{0,2})?$/.test(e.detail.value)) { //正则验证，提现金额小数点后不能大于两位数字
      money = e.detail.value;
    } else {
      money = e.detail.value.substring(0, e.detail.value.length - 1);
    }
    this.setData({
      price: money
    })
  },
  phoneChange(e){
    let val = e.detail.value.replace(/\D/g, '')
    this.setData({
      phone: val
    })
  },
  wxNumberChange(e){
    this.setData({
      wxNumber: e.detail.value
    })
  },
  // 保存购房需求信息
  saveBuyHouseInfo(){
      let data = {
        area: this.data.area,
        acre: this.data.acre,
        roomType: this.data.roomType,
        price: this.data.price,
        payType: this.data.selectPayIndex,
        phone: this.data.phone,
        wxNumber: this.data.wxNumber
      }
      if(!this.data.phone && !this.data.wxNumber){
        this.setData({
          error: '请输入电话号码或者微信号码'
        })
        return;
      }
      if(!!this.data.phone && this.data.phone.length < 11){
        this.setData({
          error: '手机号码格式有误,请检查后再次提交!'
        })
        return;
      }
      if(!this.data.area || !this.data.acre  || !this.data.price  || !this.data.roomType || this.data.payType == 3){
        this.setData({
          error: '请输入楼盘基本信息'
        })
        return;
      }
      let _this = this
      db.collection('buy_house_pass').add({
        data: data,
      }).then(res => {
        wx.showToast({
          title: '提交成功!',
          icon: 'success',
          duration: 2000
        })
        _this.setData({
          area: '',
          acre: '',
          roomType: '',
          price: null,
          isShow: true,
          selectPayIndex: 3,
          phone: ''
        })
      }).catch(err => {
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})