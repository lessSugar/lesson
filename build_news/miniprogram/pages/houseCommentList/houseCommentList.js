// miniprogram/pages/houseCommentList/houseCommentList.js
const app = getApp()
const db = wx.cloud.database({
  env: 'dev-9cjx9'
});
Page({

  /**
   * 页面的初始数据
   * avatarUrl 头像url https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83erDKjfFYKCZvc6gZF7TCbSpooqrN8QDLL9Twa5BbETv1uEtqPiaZmLx6vhicCibhTia7JLsjp1ITwcJxA/132
   */
  data: {
    userInfo: {},
    houseId: null,
    // 是否是写评论模式
    model: 0,
    commentContent: '',
    success: null,
    error: null
  },
  writeModel(){
    this.setData({
      model: 1
    })
  },
  saveComment(){
    let _this = this;
    let data = {
      houseId: this.data.houseId,
      comment_username: this.data.userInfo.nickName,
      comment_content: this.data.commentContent,
      has_check: '0',
      avatar_url: this.data.userInfo.avatarUrl
    }
    db.collection('house_info_comment').add({
      data: data,
      success(res) {
        _this.setData({
          success: '评论发表成功!'
        })
        _this.setData({
          commentContent: ''
        })
      }
    })
    this.setData({
      model: 0
    })
  },
  calcelComment(){
    this.setData({
      commentContent: '',
      model: 0
    })

  },
  changeCommentContent(e){
    this.setData({
      commentContent: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userInfo: app.globalData.userInfo,
      houseId: options.houseId
    })
    let _this = this;
    db.collection('house_info_comment').where({
      houseId: this.data.houseId,
      has_check: '1'
    }).get({
      success: function(res) {
        _this.setData({
          commentList: res.data
        })
        console.log(this.data.commentList)
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})