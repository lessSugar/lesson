// miniprogram/pages/opinion/opinion.js
const db = wx.cloud.database({
  env: 'dev-9cjx9'
})
Page({

  /**
   * 页面的初始数据
   */
  data: {
    opinion: null,
    success: null
  },  
  opinionChange(e){
      this.setData({
        opinion: e.detail.value
      })
  },
  saveOpinoin(){
    debugger
    console.log(this.data.opinion)
    if(!this.data.opinion){
      return false;
    }
    let saveData = {
      opinion: this.data.opinion
    }
    let _this = this;
    db.collection('opinoin').add({
      data: saveData,
      success(res) {
        debugger
        _this.setData({
          success: '提交成功!'
        })
        setTimeout(() => {
          _this.setData({
            opinion: null
          })
          wx.reLaunch({
            url: '/pages/mine/mine',
          })
        },2000)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})