// miniprogram/pages/showHousePage/showHousePage.js
const app = getApp()
const db = wx.cloud.database({
  env: 'dev-9cjx9'
});
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {
    houseId: 'e1422f825eca47aa0027d35102689e8e',
    houseInfo: {},
    userInfo: {},
    hasCollect: false,
    commentList: []
  },
  openCallPanel(e) {
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.phone
    })
  },
  /**
   * 用户收藏信息
   */
  collectHouse(){
    let nickName = this.data.userInfo.nickName
    let data = {
      nickName: this.data.userInfo.nickName,
      houseId: this.data.houseId
    }
    let _this = this;
    db.collection('user_collect').add({
      data: data,
      success(res) {
        wx.showToast({
          title: '收藏成功!',
          icon: 'success',
          duration: 2000
        })
        _this.setData({
          hasCollect: true
        })
      }
    })
  },
  /**
   * 用户取消收藏信息
   */
  cancelCollectHouse(){
    let nickName = this.data.userInfo.nickName
    let _this = this;
    // 查询用户是否已经收藏该房屋
    db.collection('user_collect').where({
      nickName: this.data.userInfo.nickName,
      houseId: this.data.houseId
    }).get({
      success: function(res) {
        db.collection('user_collect').doc(res.data[0]._id).remove({
          success: function(res) {
            _this.setData({
              hasCollect: false
            })
            console.log(res.data)
          }
        })
        console.log(_this.data.hasCollect)
      }
    })
  },
  /**
   * 查看房屋详情
   */
  showHouseIntrodice(){
    wx.navigateTo({
      url: `/pages/showHouseIntroduce/showHouseIntroduce?projectIntroduce=${this.data.houseInfo.projectIntroduce}`,
    })
  },
  showCommentList(){
    wx.navigateTo({
      url: `/pages/houseCommentList/houseCommentList?houseId=${this.data.houseId}`,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let _this = this;
    if(!!app.globalData.userInfo){
      this.setData({
        userInfo: app.globalData.userInfo
      })
    }else {
      wx.navigateTo({
        url: '/pages/startPage/startPage?needBack=1',
      })
    }
    let houseId = options.houseId
    if(!!houseId){
      this.setData({
        houseId: houseId
      })
    }
    db.collection('house_info_comment').where({
      houseId: this.data.houseId,
      has_check: '1'
    }).get({
      success: function(res) {
        _this.setData({
          commentList: res.data
        })
        console.log(this.data.commentList)
      }
    })
    // 查询用户是否已经收藏该房屋
    db.collection('user_collect').where({
      nickName: this.data.userInfo.nickName,
      houseId: this.data.houseId
    }).get({
      success: function(res) {
        _this.setData({
          hasCollect: res.data.length > 0
        })
        console.log(_this.data.hasCollect)
      }
    })
    db.collection('hot_sale_house_info').where({
      _id: this.data.houseId
    }).get({
      success: function(res) {
        let result = res.data[0]
        let areaCode = result.areaCode;
        let areaName = ''
        if(areaCode == '0'){
          areaName = '北三县'
        }else if(areaCode == '1'){
          areaName = '雄安新区'
        }else if(areaCode == '2'){
          areaName = '环京楼盘'
        }else if(areaCode == '3'){
          areaName = '视频看房'
        }
        result.areaCode = areaName;
        _this.setData({
          houseInfo: result
        })
        console.log(_this.data.imgs)
      }
    })
     // 当前房屋ID记录在用户本地数据,记做历史记录
     wx.getStorage({
      key: 'viewHistory',
      success (res) {
        console.log(res.data)
        let history =_this.addHistory(res.data)
        wx.setStorage({
          key: 'viewHistory',
          data: history,
        })
      },
      fail(err){
        let history = []
        _this.addHistory(history)
        wx.setStorage({
          key: 'viewHistory',
          data: history,
        })
      }
    })
  },
  addHistory(handleData){
    if(handleData.indexOf(this.data.houseId) != -1){
        handleData.splice(handleData.indexOf(this.data.houseId),1)
    }
    if(handleData.length == 10){
        handleData.pop()
    }
    handleData.unshift(this.data.houseId)
    return handleData
  },
  imgYu: function (event) {
    let src = event.currentTarget.dataset.img;    // 获取data-src
    let imgList = this.data.houseInfo.projectImgSrcs;// 获取data-list
    // 图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: imgList // 需要预览的图片http链接列表
    })
  },
  copyText: function (e) {
    console.log(e)
    wx.setClipboardData({
      data: e.currentTarget.dataset.text,
      success: function (res) {
        wx.getClipboardData({
          success: function (res) {
            wx.showToast({
              title: '复制成功'
            })
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})