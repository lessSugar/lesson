// pages/infoshow/infoshow.js
//获取小程序云开发数据库连接
const db = wx.cloud.database({
  env: 'dev-9cjx9'
});
const _ = db.command
//获取客服电话号码
const phoneNumber = require('../../utils/commonConst.js').data.customServicePhoneNumber;
Page({
  leapToDirIndex(e) {
    //跳转到对应的房产大全页面
    let leapIndex = e.currentTarget.dataset['index'];
    console.log(leapIndex)
    let url = '/pages/houseInfoDir/houseInfoDir?leapIndex=' + leapIndex;
    if(leapIndex == 4){
      url = '/pages/addHousePage/addHousePage'
    }else if(leapIndex == 3){
      url = '/pages/buyHousePass/buyHousePass'
    } else if(leapIndex == 5){
      url = '/pages/videoWatchHouse/videoWatchHouse'
    } else {
      wx.reLaunch({
        url: url
      })
    }
    wx.navigateTo({
      url: url
    })
  },
  openCallPanel() {
    wx.makePhoneCall({
      phoneNumber
    })
  },
  showHouseDetails(e){
    let houseId = e.currentTarget.dataset.id
    wx.navigateTo ({
      url: `/pages/showHousePage/showHousePage?houseId=${houseId}`,
    })
  },
  showNewsBody(e){
    let dataIndex = e.currentTarget.dataset.index
    let newsId = this.data.newsData[dataIndex]._id
    wx.navigateTo({
      url: `/pages/showNews/showNews?newsId=${newsId}`,
    })
  },
  leapToHouseList(){
    wx.reLaunch({
      url: '/pages/houseInfoDir/houseInfoDir',
    })
  },
  /**
   * 页面的初始数据
   */
  data: {
    houseList: [{
      imgSrc: '/images/11.jpg',
      name: null,
      intInfo: null,
      roomInfo: null,
      price: null,
      fetureInfos: [],
      totalPrice: null,
      areaInfo: [],
    }],
    dbinfo: '',
    btns: [{
      id: 1,
      btnName: '北三县',
      imgSrc: '/images/012.png'
    }, {
      id: 2,
      btnName: '雄安新区',
      imgSrc: '/images/022.png'
    }, {
      id: 3,
      btnName: '环京楼盘',
      imgSrc: '/images/033.png'
    }, {
      id: 4,
      btnName: '购房通道',
      imgSrc: '/images/044.png'
    }, {
      id: 5,
      btnName: '经纪人通道',
      imgSrc: '/images/055.png'
    }, {
      id: 6,
      btnName: '视频看房',
      imgSrc: '/images/066.png'
    }],
    newsData: [],
    isTiptrue: false
  },
  imgYu: function (event) {
    let src = event.currentTarget.dataset.img;    // 获取data-src
    let imgList = this.data.scrollImgs;// 获取data-list
    // 图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: imgList // 需要预览的图片http链接列表
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let _this = this;
    db.collection('news').where({})
    .get({
      success: function(res) {
        _this.setData({
          newsData: res.data
        })
        console.log(this.data.newsData)
      }
    })
    db.collection('hot_sale_house_info').where({
        hot: '1'
    }).get({
        success: function(res) {
          _this.setData({
            houseList: res.data
          })
          console.log(_this.data.listData)
        }
    })
    db.collection('config_info').where({})
    .get({
      success: function(res) {
        _this.setData({
          scrollImgs: res.data[0].indexImgs
        })
      }
    })
     // onLoad中添加以下代码
    let firstOpen = wx.getStorageSync("loadOpen")    
    if (firstOpen == undefined || firstOpen == '') { // 根据缓存周期决定是否显示新手引导      
      this.setData({ isTiptrue: true })    
    } 
    else {      
      this.setData({ isTiptrue: false })    
    }
  },
  // Page中添加关闭引导
  closeGuide: function (e) {    
    wx.setStorage({      
      key: 'loadOpen',      
      data: 'OpenTwo'    
    })    
    this.setData({ isTiptrue: false }) 
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
newsBtnClick() {
    wx.navigateTo({
      url: '/pages/news/news'
    });
  }
})