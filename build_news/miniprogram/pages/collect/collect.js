// miniprogram/pages/collect/collect.js
const app = getApp()
const db = wx.cloud.database({
  env: 'dev-9cjx9'
});
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: '',
    houseList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  showHouseDetails(e){
    let houseId = e.currentTarget.dataset.id
    wx.navigateTo ({
      url: `/pages/showHousePage/showHousePage?houseId=${houseId}`,
    })
  },
  onLoad: function (options) {
    let _this = this;
    this.setData({
      userInfo: app.globalData.userInfo
    })
    let nickName = this.data.userInfo.nickName
    let data = {
      nickName: nickName,
    }
    db.collection('user_collect').where({
      nickName: this.data.userInfo.nickName,
    }).get({
      success: function(res) {
        let collectIds = []
        for(let i=0;i < res.data.length;i++){
          collectIds.push(res.data[i].houseId)
        }
        if(collectIds.length != 0){
          db.collection('hot_sale_house_info').where({
              _id: _.in(collectIds)
          }).get({
              success: function(res) {
                _this.setData({
                  houseList: res.data
                })
                console.log(_this.data.listData)
              }
          })
            console.log(_this.data.hasCollect)
          }
    }

    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})