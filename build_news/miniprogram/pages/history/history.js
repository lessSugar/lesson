// miniprogram/pages/history/history.js
const db = wx.cloud.database({
  env: 'dev-9cjx9'
})
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {
    houseList: [],
    handleModel: 0,
    dialogShow: false,
    buttons: [{text: '取消'}, {text: '确定'}],
  },
  tapDialogButton(e) {
    let index = e.detail.index
    if(index == 1){
      wx.removeStorage({
        key: 'viewHistory',
      })
      this.setData({
        houseList: []
      })
    }
    this.setData({
        dialogShow: false,
    })
  },
  clearHistory(){
    this.setData({
      dialogShow: true
    })

  },
  handleEdit(){
    this.setData({
      handleModel: 1
    })
  },
  handleOver(){
    this.setData({
      handleModel: 0
    })
  },
  delItem(e){
    let _this = this
    let itemIndex = e.currentTarget.dataset.index
    // 当前房屋ID记录在用户本地数据,记做历史记录
    wx.getStorage({
    key: 'viewHistory',
    success (res) {
      console.log(res.data)
      wx.getStorage({
        key: 'viewHistory',
        success (res) {
          let localHistory = res.data
          localHistory.splice(itemIndex,1)
          wx.setStorage({
            key: 'viewHistory',
            data: localHistory,
          })
          _this.data.houseList.splice(itemIndex,1)
          _this.setData({
            houseList: _this.data.houseList
          })
        }
      })
    },
    fail(err){
      let history = []
      _this.addHistory(history)
      wx.setStorage({
        key: 'viewHistory',
        data: history,
      })
    }
  })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let _this = this
    wx.getStorage({
      key: 'viewHistory',
      success (res) {
        let houseIds = res.data
        db.collection('hot_sale_house_info').where({
          // gt 方法用于指定一个 "大于" 条件，此处 _.gt(30) 是一个 "大于 30" 的条件
              _id: _.in(houseIds)
            })
            .get({
              success: function(res) {
                _this.setData({
                  houseList: res.data
                })
                console.log(_this.data.listData)
              }
            })
      }
    })
    
  },
  showHouseDetails(e){
    let houseId = e.currentTarget.dataset.id
    if(this.data.handleModel != 1){
      wx.navigateTo ({
        url: `/pages/showHousePage/showHousePage?houseId=${houseId}`,
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})