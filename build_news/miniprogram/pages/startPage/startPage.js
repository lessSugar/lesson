// miniprogram/pages/startPage/startPage.js
const app = getApp()
Page({
  getUserInfo(e){
    console.log(e)
    let tempUserInfo = e.detail.userInfo
    if(!!tempUserInfo){
      app.globalData.userInfo = tempUserInfo
      this.setData({
        userInfo: e.detail.userInfo,
        hasUserInfo: true
      })
      wx.reLaunch({
        url: '/pages/index/index'
      })
    }
  },
  /**
   * 页面的初始数据
   */
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    needBack: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let needBack = options.needBack
    if(!!needBack){
      this.setData({
        needBack: needBack
      })
    }
    let _this = this
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    }  else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          debugger
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
          if(!this.data.needBack){
            wx.reLaunch({
              url: '/pages/index/index'
            })
          }else {
            let pages = getCurrentPages();
            let prePage = pages[pages.length - 2];
            wx.navigateBack({
              delta:1
            })
          }
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    let userInfo = app.globalData.userInfo
    if(!userInfo || !userInfo.avatarUrl){
      wx.reLaunch({
        url: '/pages/index/index',
      })
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})