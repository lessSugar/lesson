// miniprogram/pages/houseInfoDir/houseInfoDir.js
const db = wx.cloud.database({
  env: 'dev-9cjx9'
})
const _ = db.command
Page({
  /**
   * 页面的初始数据
   */
  data: {
    activeLeftItem:"1",
    houseList: [],
  },
  showHouseDetails(e){
    let houseId = e.currentTarget.dataset.id
    wx.navigateTo({
      url: `/pages/showHousePage/showHousePage?houseId=${houseId}`,
    }) 
  },
  search(value){
    let _this = this
    return new Promise((resolve, reject) => {
      db.collection('hot_sale_house_info').where(
        _.or([{
          houseName: {
            $regex:'.*' + value + '.*',	
            $options: 'i'	
          }
        }, {
          houseAddr: {							
            $regex:'.*' + value + '.*',	
            $options: 'i'			
          }
        }])
      ).get({
            success: function(res) {
              _this.setData({
                houseList: res.data,
                activeLeftItem: 4
              })
              resolve([])
              console.log(_this.data.listData)
            },
          })
    })
  },
  clearSearch(){
    this.setData({
      activeLeftItem: 0
    })
    let _this = this
    db.collection('hot_sale_house_info').where({
      // gt 方法用于指定一个 "大于" 条件，此处 _.gt(30) 是一个 "大于 30" 的条件
          areaCode: _.eq(this.data.activeLeftItem.toString())
        })
        .get({
          success: function(res) {
            _this.setData({
              houseList: res.data
            })
            console.log(_this.data.listData)
          }
    })
  },
  selectResult: function (e) {
    console.log('select result', e.detail)
  },
  changeActiveItem(e){
    let _this = this;
    this.setData({
      activeLeftItem: e.currentTarget.dataset['index']
    })
    db.collection('hot_sale_house_info').where({
  // gt 方法用于指定一个 "大于" 条件，此处 _.gt(30) 是一个 "大于 30" 的条件
      areaCode: _.eq(this.data.activeLeftItem)
    })
    .get({
      success: function(res) {
        _this.setData({
          houseList: res.data
        })
        console.log(_this.data.listData)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      search: this.search.bind(this)
    })
    let leapIndex = Number(options['leapIndex']);
    if(!leapIndex){
      leapIndex = "0";
    }
    let _this = this;
    this.setData({
      activeLeftItem: leapIndex
    })
    db.collection('hot_sale_house_info').where({
      // gt 方法用于指定一个 "大于" 条件，此处 _.gt(30) 是一个 "大于 30" 的条件
          areaCode: _.eq(this.data.activeLeftItem.toString())
        })
        .get({
          success: function(res) {
            _this.setData({
              houseList: res.data
            })
            console.log(_this.data.listData)
          }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})